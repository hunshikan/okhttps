# OkHttps Stomp

#### 介绍

基于 OkHttps 的同文件多线程并行下载方案


##### Maven

```xml
<dependency>
     <groupId>com.ejlchina</groupId>
     <artifactId>okhttps-parallel</artifactId>
     <version>2.5.1</version>
</dependency>
```

##### Gradle

```groovy
implementation 'com.ejlchina:okhttps-stomp:2.5.1'
```

#### 更多请参阅 [http://okhttps.ejlchina.com/](http://okhttps.ejlchina.com/)
